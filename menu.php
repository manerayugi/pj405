<meta charset="utf-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--
Dragonfruit Template
http://www.templatemo.com/preview/templatemo_411_dragonfruit
-->
<meta name="description" content="" />
<!-- templatemo 411 dragonfruit -->
<meta name="author" content="templatemo">
<!-- Favicon-->
<link rel="shortcut icon" href="images/imagesP1.jpg" />
<!-- Font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- Camera -->
<link href="css/camera.css" rel="stylesheet">
<!-- Template  -->
<link href="css/templatemo_style.css" rel="stylesheet">

<div id="templatemo_mobile_menu">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="#templatemo_banner_slide"><i class="glyphicon glyphicon-home"></i> &nbsp; หน้าหลัก</a></li>
            <li><a href="#templatemo_about"><i class="glyphicon glyphicon-briefcase"></i> &nbsp; วิกิภาษาเพิร์ล</a></li>
            <li><a href="http://www.activestate.com/" target="blank" class="external-link"><i class="glyphicon glyphicon-download"></i> &nbsp; ดาวน์โหลดโปรแกรม</a></li>
            <li><a href="#templatemo_contact"><i class="glyphicon glyphicon-user"></i> &nbsp; ผู้พัฒนา</a></li>
            <li><a href="tester.php" target="blank" class="external-link"><i class="glyphicon glyphicon-download"></i> &nbsp; TESTER</a></li>
        </ul>
</div>
<div class="container_wapper">
    <div id="templatemo_banner_menu">
        <div class="container-fluid">
            <div class="col-xs-4 templatemo_logo">
            	<a href="#">
                	<img src="images/imagesP1.jpg" id="logo_img" width="90px" height="90px" alt="dragonfruit website template" title="Dragonfruit Template" />
                	<h1 id="logo_text">Pop<span>Perl</span></h1>
                </a>
            </div>
            <div href="#" class="col-sm-8 hidden-xs">
                <ul class="nav nav-justified ">
                    <li><a href="#templatemo_banner_slide">หน้าหลัก</a></li>
                    <li><a href="#templatemo_about" >วิกิภาษาเพิร์ล</a></li>
                    <li><a href="http://www.activestate.com/" target="blank" class="external-link" >ดาวน์โหลดโปรแกรม</a></li>
                    <li><a href="#templatemo_contact" >ผู้พัฒนา</a></li>
                    <li><a href="tester.php" target="blank" class="external-link" >TESTER</a></li>
                 </ul>
            </div>
            <div class="col-xs-8 visible-xs">
                <a href="#" id="mobile_menu"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.singlePageNav.min.js"></script>
<script src="js/unslider.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script src="js/templatemo_script.js"></script>
