<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--
    Dragonfruit Template
    http://www.templatemo.com/preview/templatemo_411_dragonfruit
    -->
    <title>ยินดีต้อนรับสู่บทเรียนภาษาเพิร์ล</title>
    <meta name="description" content="" />
    <!-- templatemo 411 dragonfruit -->
    <meta name="author" content="templatemo">
    <!-- Favicon-->
    <link rel="shortcut icon" href="images/imagesP1.jpg" />
    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Camera -->
    <link href="css/camera.css" rel="stylesheet">
    <!-- Template  -->
    <link href="css/templatemo_style.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
<div class="banner" id="templatemo_banner_slide">
    <ul>
        <li class="templatemo_banner_slide_01">
            <div class="slide_caption">
            </div>
        </li>
        <li class="templatemo_banner_slide_02">
            <div class="slide_caption">
            </div>
        </li>
        <li class="templatemo_banner_slide_03">
            <div class="slide_caption">
            </div>
        </li>
    </ul>
</div>

<div id="templatemo_mobile_menu">
        <ul class="nav nav-pills nav-stacked">
            <li><a href="#templatemo_banner_slide"><i class="glyphicon glyphicon-home"></i> &nbsp; หน้าหลัก</a></li>
            <li><a href="#templatemo_about"><i class="glyphicon glyphicon-briefcase"></i> &nbsp; วิกิภาษาเพิร์ล</a></li>
            <li><a href="http://www.activestate.com/" target="blank" class="external-link"><i class="glyphicon glyphicon-download"></i> &nbsp; ดาวน์โหลดโปรแกรม</a></li>
            <li><a href="#templatemo_contact"><i class="glyphicon glyphicon-user"></i> &nbsp; ผู้พัฒนา</a></li>
            <li><a href="tester.php" target="blank" class="external-link"><i class="glyphicon glyphicon-download"></i> &nbsp; TESTER</a></li>
        </ul>
</div>
<div class="container_wapper">
    <div id="templatemo_banner_menu">
        <div class="container-fluid">
            <div class="col-xs-4 templatemo_logo">
            	<a href="#">
                	<img src="images/imagesP1.jpg" id="logo_img" width="90px" height="90px" alt="dragonfruit website template" title="Dragonfruit Template" />
                	<h1 id="logo_text">Pop<span>Perl</span></h1>
                </a>
            </div>
            <div href="" class="col-sm-8 hidden-xs">
                <ul class="nav nav-justified ">
                    <li><a href="#templatemo_banner_slide">หน้าหลัก</a></li>
                    <li><a href="#templatemo_about" >วิกิภาษาเพิร์ล</a></li>
                    <li><a href="http://www.activestate.com/" target="blank" class="external-link" >ดาวน์โหลดโปรแกรม</a></li>
                    <li><a href="#templatemo_contact" >ผู้พัฒนา</a></li>
                    <li><a href="tester.php" target="blank" class="external-link" >TESTER</a></li>
                 </ul>
            </div>
            <div class="col-xs-8 visible-xs">
                <a href="#" id="mobile_menu"><span class="glyphicon glyphicon-th-list"></span></a>
            </div>
        </div>
    </div>
</div>
<!--วิกิภาษาเพิร์ล-->
<div id="templatemo_about" class="container_wapper">
    <div class="container-fluid">
        <h1>วิกิภาษาเพิร์ล</h1>
		<a href="IntroToPerl.html">
        <div class="col-sm-6 col-md-3 about_icon">
            <div class="imgwap brownred"><i class="fa fa-terminal"></i></div></a>
            <h2>รู้จักกับภาษาเพิร์ล</h2>
            <p>มาดูกันว่า ภาษาเพิร์ลคืออะไร เอาไปใช้ทำอะไรได้บ้าง นิยมมากกันขนาดไหน</p>
        </div>

        <a href="BasicVariable.html">
        <div class="col-sm-6 col-md-3 about_icon">
            <div class="imgwap blue"><i class="fa fa-bold" ></i></div></a>
            <h2>ตัวแปรพื้นฐาน</h2>
            <p>ตัวแปร คือโครงสร้างพื้นฐานที่ควรรู้สำหรับการเริ่มต้นเขียนโปรแกรม เพื่อให้สามารถเขียนโปรแกรมที่ซับซ้อนมากขึ้นตามที่เราชอบ</p>
        </div>

        <a href="Operation.html">
        <div class="col-sm-6 col-md-3 about_icon">
            <div class="imgwap yellow"><i class="fa fa-asterisk"></i></div></a>
            <h2>ตัวดำเนินการ</h2>
            <p>บวก ลบ คูณ หาร พวกนี้จัดเป็นตัวดำเนินการทางคณิตศาสตร์ รวมไปถึงตัวดำเนินการทางตรรกะ ซึ่งภาษาโปรแกรมระดับสูงพึงต้องมี ภาษาเพิร์ลก็มีเหมือนกัน มาดูกันว่ามันเขียนอย่างไร</p>
        </div>

        <a href="Structure.html">
        <div class="col-sm-6 col-md-3 about_icon">
            <div class="imgwap statistic"><i class="fa fa-comments"></i></div></a>
    	<h2>โครงสร้างควบคุมการทำงานของโปรแกรม</h2>
            <p>ในบทนี้ ผู้เรียนจะได้เรียนรู้เกี่ยวกับโครงสร้างการทำงานของโปรแกรม เพื่อกำหนดทิศทางการทำงานของโปรแกรม</p>
        </div>
        <div class="clearfix testimonial_top_bottom_spacer"></div>
		<p align="middle"><font size="75%"><a href="LessonPage.html"><img="images/blue-button-hi.jpg" >>ดูเพิ่มเติม<</a></font></p>

        <div id="templatemo_contact" class="container_wapper">
    <div class="container-fluid">
        <h1>About Our Developer</h1>
        <div class="clearfix testimonial_top_bottom_spacer"></div>
        <div class="col-xs-1 pre_next_wap" id="prev_testimonial">
            <a href="#"><span class="glyphicon glyphicon-chevron-left pre_next"></span></a>
        </div>
        <div id="testimonial_text_wap" class="col-xs-9 col-sm-10">
              <div class="testimonial_text">
                <div class="col-sm-3">
                    <img src="images/aof.jpg" class="img-responsive" alt="Business Development Manager" />
                </div>
                <div class="col-sm-9">
                    <a href="https://www.facebook.com/activesmart" target="blank"><h2>Jeeradech  Sonkosa</h2></a>
                    <h3>Master of Developer</h3>
                    <p>560610528<br>Department of Computer Engineering, Faculty of Engineering, Chiang Mai University</p>
                </div>
            </div><!--.testimonial_text-->
            <div class="testimonial_text">
                <div class="col-sm-3">
                    <img src="images/mee.jpg" class="img-responsive" alt="Chief Public Relation Officer" />
                </div>
                <div class="col-sm-9">
                    <a href="https://www.facebook.com/pittayathon.tritusplagalis" target="blank"><h2>Pittayathon  Rinkaewngam</h2></a>
                    <h3>Co-Master of Developer</h3>
                    <p>560610564<br>Department of Computer Engineering, Faculty of Engineering, Chiang Mai University</p>
                </div>
            </div><!--.testimonial_text-->
            <div class="testimonial_text">
                <div class="col-sm-3">
                    <img src="images/oil.jpg" class="img-responsive" alt="Marketing Executive" />
                </div>
                <div class="col-sm-9">
                    <a href="https://www.facebook.com/vezpa.hale" target="blank"><h2>Varangkana  Rouhnsupa</h2></a>
                    <h3>Development Assistant</h3>
                    <p>560610576<br>Department of Computer Engineering, Faculty of Engineering, Chiang Mai University</p>
                </div>
            </div><!--.testimonial_text-->
            <div class="testimonial_text">
                <div class="col-sm-3">
                    <img src="images/aj.jpg" class="img-responsive" alt="Marketing Executive" />
                </div>
                <div class="col-sm-9">
                    <a href="https://www.facebook.com/kenshiro.daisensei?fref=ts" target="blank"><h2>Asst. Prof.Narathip Tiangtae,Ph.D.</h2></a>
                    <h3>Project adviser</h3>
                    <p>Lecturer<br>Department of Computer Engineering, Faculty of Engineering, Chiang Mai University</p>
                </div>
            </div><!--.testimonial_text-->
         </div><!--#testimonial_text_wap-->
        <div class="col-xs-1 pre_next_wap" id="next_testimonial">
            <a href="#"><span class="glyphicon glyphicon-chevron-right pre_next"></span></a>
        </div>
        <div class="clearfix testimonial_top_bottom_spacer"></div>
    </div>
</div>
<div id="templatemo_footer">
    <div>
        <p id="footer">Copyright 2016 &copy; PopPerl All Right Reserved.</p>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.singlePageNav.min.js"></script>
<script src="js/unslider.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script src="js/templatemo_script.js"></script>
</body>
</html>
